import java.util.Scanner;

class Fahrkartenautomat1
{
	public static void main(String[] args)
    {
       try (Scanner myScanner = new Scanner(System.in)) {
	}
                         
       double zuZahlenderBetrag = fahrkartenbestellungErfassenBetrag();
       
       double zuZahlenderBetragGesamt = fahrkartenbestellungErfassenAnzahl(zuZahlenderBetrag);
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetragGesamt);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetragGesamt);
      
    }
	public static double fahrkartenbestellungErfassenBetrag() {
		try (Scanner myScanner = new Scanner(System.in)) {
			double zuZahlenderBetrag = 0;
			double Auswahl1;
			double Betrag;
			
			System.out.println("Fahrkartenbestellvorgang");
			System.out.println("------------------------");
			
			System.out.println("\nBitte w�hlen Sie ein Ticket");
			System.out.println("\n(1) Berlin AB");
			System.out.println("(2) Berlin BC");
			System.out.println("(3) Berlin ABC");
			
			System.out.print("\nIhre Auswahl: ");
			
			Auswahl1 = myScanner.nextDouble();
			if (Auswahl1 == 1) {
				System.out.println("W�hlen Sie Ihre Tarifstufe f�r den Bereich Berlin AB aus: ");
				System.out.println("(1) Regeltarif: 3,00 Euro");
				System.out.println("(2) Erm��igt: 1,90 Euro");
				System.out.println("\nIhre Auswahl: ");
				
				Betrag = myScanner.nextDouble();
				
				if(Betrag == 1) {
					zuZahlenderBetrag += 3.00;
				}
				if(Betrag == 2) {
					zuZahlenderBetrag += 1.90;
				}
				if(Betrag < 1 || Betrag > 2) {
					System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
					
				}
			}
			if (Auswahl1 == 2) {
				System.out.println("W�hlen Sie Ihre Tarifstufe f�r den Bereich Berlin BC aus: ");
				System.out.println("(1) Regeltarif: 3,50 Euro");
				System.out.println("(2) Erm��igt: 2,40 Euro");
				System.out.println("\nIhre Auswahl: ");
				
				Betrag = myScanner.nextDouble();
				
				if(Betrag == 1) {
					zuZahlenderBetrag += 3.50;
				}
				if(Betrag == 2) {
					zuZahlenderBetrag += 2.40;
				}
				if(Betrag < 1 ||Betrag > 2) {
					System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
				}
			}
			if (Auswahl1 == 3) {
				System.out.println("W�hlen Sie Ihre Tarifstufe f�r den Bereich Berlin ABC aus: ");
				System.out.println("(1) Regeltarif: 3,80 Euro");
				System.out.println("(2) Erm��igt: 2,70 Euro");
				System.out.println("\nIhre Auswahl: ");
				
				Betrag = myScanner.nextDouble();
				
				if(Betrag == 1) {
					zuZahlenderBetrag += 3.80;
				}
				if(Betrag == 2) {
					zuZahlenderBetrag += 2.70;
				}
				if(Betrag < 1 || Betrag > 2) {
					System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
				}
			}
			if (Auswahl1 < 1 ||Auswahl1 > 3) {
				System.out.println("Geben Sie bitte eine Option ein, die Ihnen zur Auswahl steht.");
				
				fahrkartenbestellungErfassenBetrag();
			}
				    	    
			return zuZahlenderBetrag;
		}
	}
		
	public static double fahrkartenbestellungErfassenAnzahl(double zuZahlenderBetrag) {
		try (Scanner myScanner = new Scanner(System.in)) {
			double anzahlFahrkarten;
			
			System.out.print("Anzahl der Tickets: ");
			anzahlFahrkarten = myScanner.nextByte();
			
			if (anzahlFahrkarten > 10 || anzahlFahrkarten < 1) {
				anzahlFahrkarten = 1;
				System.out.println("Sie haben eine unzul�ssige Fahrkartenanzahl eingegeben. Es wird mit dem Standardwert 1  weitergerechnet.");
			}
				
			return anzahlFahrkarten * zuZahlenderBetrag;
		}
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetragGesamt) {
		try (Scanner myScanner = new Scanner(System.in)) {
			double eingezahlterGesamtbetrag = 0.0;
			double eingeworfeneMuenze;
			
			while(zuZahlenderBetragGesamt >= eingezahlterGesamtbetrag)
			  {
			   System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", (zuZahlenderBetragGesamt - eingezahlterGesamtbetrag),  " Euro");
			   System.out.print("Eingabe (mind. 5 Ct, h�chstens 2 Euro): ");
			   eingeworfeneMuenze = myScanner.nextDouble();
			   eingezahlterGesamtbetrag += eingeworfeneMuenze;
			  }
			return eingezahlterGesamtbetrag;
		}
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetragGesamt) {
				
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetragGesamt;
	       if(rueckgabebetrag > 0.0)
	       {
	    	   System.out.printf("%s%.2f%s%n", "Der R�ckgabebetrag in H�he von ", rueckgabebetrag, " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2,00 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1,00 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	           while(rueckgabebetrag >= 0.02)// 2 CENT-M�nzen
	           {
	        	  System.out.println("2 CENT");
	 	          rueckgabebetrag -= 0.02;
	           }
	           while(rueckgabebetrag >= 0.01)// 1 CENT-M�nzen
	           {
	        	  System.out.println("1 CENT");
	 	          rueckgabebetrag -= 0.01;
	           }
	           
	       }
	     
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir w�nschen Ihnen eine gute Fahrt.");
	         
	}
	  
		
	}