import java.util.Scanner;

class Whileaufgaben2
{
	  public static void main (String[] args )
	  {
	    try (Scanner myScanner = new Scanner( System.in )) {
			long n, fakultaet = 1;

			System.out.println( "Geben Sie N ein:" );
			n = myScanner.nextLong();

			if ( n >= 0 )
			{
			  while ( n > 1 )
			  {
			    fakultaet = fakultaet * n;
			    n   = n - 1;
			  }
			  System.out.println( "Fakultaet ist " + fakultaet );
			}
			else
			{
			  System.out.println("N muss 0 oder groesser sein");
			}
		}
	  }
	}