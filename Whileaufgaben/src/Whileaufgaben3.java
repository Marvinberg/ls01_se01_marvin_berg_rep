import java.util.Scanner;
public class Whileaufgaben3 {

		public static void main(String[] args) {
			
			
			int Zahl = 0;
			int Quersumme  = 0;
			
			try (Scanner scan = new Scanner(System.in)) {
				System.out.printf("Zahl eingeben: ");
				Zahl = scan.nextInt();
			}
			
			while (Quersumme != (Zahl / 10) + (Zahl % 10)){
				Quersumme = (Zahl / 10) + (Zahl % 10);
			}
				System.out.printf("Die Quersumme von %d ergibt %d", Zahl, Quersumme);
		}
	}