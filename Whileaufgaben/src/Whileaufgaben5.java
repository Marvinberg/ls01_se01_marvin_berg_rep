import java.util.Scanner;
 
 //!Ohne While! mit while habe ich es nicht geschaft

public class Whileaufgaben5 {

 
     
    public static void main(String[] args) {
        try (var myScanner = new Scanner(System.in)) {
			
			double eingabeGeld, Zinssatz;
			int eingabeJahre;
			String ausgabe;
			double result = 0, zinsen;
			 
			//Aufgabe 5 (Jahreszins 0,014 = 1,4%)

			    System.out.print("Geldbetrag in Euro: ");
			    eingabeGeld = myScanner.nextDouble();
			    System.out.print("Jahreszins (0,05 = 5%): ");
			    Zinssatz = myScanner.nextDouble();
			    System.out.print("Laufzeit in Jahren: ");
			    eingabeJahre = myScanner.nextInt();
			     
			    for(int i=1; i<eingabeJahre+1;i++){
			         
			         
			        if(i==1){
			            zinsen = eingabeGeld*Zinssatz;
			            result = Math.round((eingabeGeld+zinsen)*100.)/100.;
			            ausgabe = "Wert nach "+i+" Jahr: "+result+"�";
			            System.out.println(ausgabe);
			        }else{
			            zinsen = result*Zinssatz;
			            result = Math.round((result+zinsen)*100.)/100.;
			            ausgabe = "Wert nach "+i+" Jahren: "+result+"�";
			            System.out.println(ausgabe);
			        }
			    }
		}
    }
 
}